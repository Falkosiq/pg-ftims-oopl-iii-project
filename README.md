# Author: 

## Jakub Falkowski

# Description

Simple ZOO simulator. 

# How to compile and run

* Import repository to your Java IDE
* Add jsfml.jar to referenced libraries (this should affect your .classpath file by adding jsfml.jar path to it)
* Compile and run project

# Usage

## Animals

You can buy animals that will attract people to your ZOO. Every kind of animal has its unique price, popularity and nutritional needs. What is more, they've got different food preferences (carnivores and herbivores).
To buy an animal simply click on the a button in a right row:

* 1 row - Lion. Cost: 1500, carnivore, needs 3 meat/day, popularity: 50
* 2 row - Giraffe. Cost: 1000, herbivore, needs 3 plants/day, popularity: 40
* 3 row - Seal. Cost: 800, carnivore, needs 2 meat/day, popularity: 30
* 4 row - Penguin. Cost: 600, carnivore, needs 1 meat/day, popularity: 25
* 5 row - Peacock. Cost: 800, herbivore, needs 1 plant/day, popularity: 20
* 6 row - Crocodile. Cost: 1200, carnivore, needs 4 meat/day, popularity: 45

## Employees

You'll also need employees - receptionists to handle your visitors and keepers to take care of your animals.
To employ an employee simply click on a plus button in a right row:

* 7 row - Receptionist. Cost: 150/day, can handle 280 visitors per day
* 8 row - Keeper. Cost: 250/day, can take care of 6 animals

## Food

You have to feed your animals to keep them alive. That's why you need food.
Click "Buy Food" button to buy as much food as needed. If you can't afford you'll buy as much as you can. 
Remember to buy fresh food everyday.

* Meat unit price: 100
* Plant unit price: 50

## Next day

Press "Next Day" button to see the results of your work and continue running your business.
You can see what's happening in your ZOO in console.
You'll start with 5000 units of money, 5 unit of meat and 5 units of plant. Try to fill all of your animal and employee slots to earn a lot of virtual money!
