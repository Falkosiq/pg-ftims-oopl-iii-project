package zoo;

public class Mammal extends Animal {

	public Mammal() {
	}

	@Override
	public String getInfo() {
		return "Mammals (class Mammalia) are any members of a clade of endothermic amniotes distinguished from reptiles"
				+ " and birds by the possession of a neocortex (a region of the brain), hair,[a] three middle ear bones,"
				+ " and mammary glands. The mammalian brain regulates body temperature and the circulatory system,"
				+ " including the four-chambered heart.";
	}
}
