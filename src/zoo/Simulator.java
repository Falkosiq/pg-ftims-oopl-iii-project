package zoo;

import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Random;
import org.jsfml.graphics.Color;
import org.jsfml.graphics.RenderWindow;
import org.jsfml.system.Vector2f;
import org.jsfml.window.Mouse;
import org.jsfml.window.VideoMode;
import org.jsfml.window.event.Event;

public class Simulator {
	private int money;
	private int meat;
	private int plants;
	public LinkedList<Animal> animalList;
	public HashSet<Worker> workers;
	private RenderWindow window;
	private Render render;

	public Simulator(int money) {
		this.money = money;
		this.meat = 5;
		this.plants = 5;
		this.animalList = new LinkedList<Animal>();
		this.workers = new HashSet<Worker>();
		this.render = new Render(this);

		window = new RenderWindow();
		window.create(new VideoMode(1024, 768), "Zoo Simulator");
		window.setFramerateLimit(30);
	}

	public void run() throws WrongTextureSize {
		while (window.isOpen()) {
			window.clear(Color.WHITE);
			render.update(window, animalList);
			render.drawText(window, "Money: ", money, 820, 36);
			render.drawText(window, "Meat: ", meat, 820, 72);
			render.drawText(window, "Plants: ", plants, 820, 108);
			window.display();

			for (Event event : window.pollEvents()) {
				if (event.type == Event.Type.CLOSED) {
					window.close();
				} else if (event.type == Event.Type.MOUSE_BUTTON_PRESSED) {
					Vector2f axis = new Vector2f(Mouse.getPosition(window));
					render.click(axis, animalList);
				}
			}
		}
	}

	public void nextDay() {
		int keeperQuantity = 0;
		int receptionistQuantity = 0;
		Iterator<Worker> iterator = workers.iterator();
		while (iterator.hasNext()) {
			if (iterator.next().getClass() == Keeper.class) {
				keeperQuantity++;
			} else {
				receptionistQuantity++;
			}
		}
		int count = 0;
		LinkedList<Animal> dead = new LinkedList<Animal>();
		for (Animal animal : animalList) {
			if (count == 6) {
				keeperQuantity--;
				count = 0;
			}
			if (animal.isCarnivore()) {
				if (animal.foodNecessity <= meat && keeperQuantity > 0) {
					animal.eat();
					meat -= animal.foodNecessity;
				} else {
					animal.die();
					dead.add(animal);
				}
			} else {
				if (animal.foodNecessity <= plants && keeperQuantity > 0) {
					animal.eat();
					plants -= animal.foodNecessity;
				} else {
					animal.die();
					dead.add(animal);
				}
			}
			count++;
		}
		for (int i = 0; i < dead.size(); i++) {
			animalList.remove(dead.get(i));
		}
		LinkedList<Worker> quit = new LinkedList<Worker>();
		for (Worker worker : workers) {
			worker.work(this);
			if (worker.isQuitting()) {
				quit.add(worker);
			}
		}
		for (int i = 0; i < quit.size(); i++) {
			workers.remove(quit.get(i));
		}
		int receptionistOrder = 0;
		int keeperOrder = 0;
		if (quit.size() > 0) {
			for (Worker worker : workers) {
				if (worker.getClass() == Receptionist.class) {
					worker.getSprite().setPosition(120 + 70 * receptionistOrder, 600);
					worker.setOrder(receptionistOrder);
					receptionistOrder++;
				} else if (worker.getClass() == Keeper.class) {
					worker.getSprite().setPosition(120 + 70 * keeperOrder, 674);
					worker.setOrder(keeperOrder);
					keeperOrder++;
				}
			}
		}
		int visitors = 0;
		Random generator = new Random();
		for (Animal animal : animalList) {
			visitors += animal.popularity + generator.nextInt(20);
		}
		if (visitors > receptionistQuantity * 280) {
			visitors = receptionistQuantity * 280;
		}
		money += visitors * 20;
		System.out.println("Your zoo was visited by " + visitors + " people this day.");
		System.out.println("You've earned " + visitors * 20 + ".");
	}

	public void spendMoney(int value) {
		money -= value;
	}

	public boolean canAfford(int value) {
		return money >= value;
	}

	public int getMeatQuantity() {
		return meat;
	}

	public int getPlantsQuantity() {
		return plants;
	}

	public void buyMeat(int quantity) {
		meat += quantity;
	}

	public void buyPlants(int quantity) {
		plants += quantity;
	}
}
