package zoo;

import java.io.IOException;
import java.nio.file.Paths;
import org.jsfml.graphics.Sprite;
import org.jsfml.graphics.Texture;
import org.jsfml.system.Vector2f;

public class AddAnimalButton implements Button {

	private Simulator simulator;
	private Texture texture;
	private Sprite sprite;
	private Class<?> animal;
	public int y;

	public AddAnimalButton(Simulator simulator, int y, Class<?> animal) {
		this.simulator = simulator;
		this.y = y;
		this.animal = animal;
		this.texture = new Texture();
		try {
			texture.loadFromFile(Paths.get("sprites/add.png"));
		} catch (IOException ex) {
			ex.printStackTrace();
		}
		this.sprite = new Sprite(texture);
		this.sprite.setOrigin(Vector2f.div(new Vector2f(this.texture.getSize()), 2));
		this.sprite.setPosition(50, y);
	}

	public void onClick() {
		int order = 0;
		for (Animal ex : simulator.animalList) {
			if (ex.getClass() == animal) {
				order++;
			}
		}
		if (order < 10) {
			if (animal == Lion.class) {
				if (simulator.canAfford(1500)) {
					simulator.animalList.add(new Lion(50, 3, order));
					simulator.spendMoney(1500);
				} else {
					System.out.println("You can't afford it.");
				}
			} else if (animal == Giraffe.class) {
				if (simulator.canAfford(1000)) {
					simulator.animalList.add(new Giraffe(40, 3, order));
					simulator.spendMoney(1000);
				} else {
					System.out.println("You can't afford it.");
				}
			} else if (animal == Seal.class) {
				if (simulator.canAfford(800)) {
					simulator.animalList.add(new Seal(30, 2, order));
					simulator.spendMoney(800);
				} else {
					System.out.println("You can't afford it.");
				}
			} else if (animal == Penguin.class) {
				if (simulator.canAfford(600)) {
					simulator.animalList.add(new Penguin(25, 1, order));
					simulator.spendMoney(600);
				} else {
					System.out.println("You can't afford it.");
				}
			} else if (animal == Peacock.class) {
				if (simulator.canAfford(800)) {
					simulator.animalList.add(new Peacock(20, 1, order));
					simulator.spendMoney(800);
				} else {
					System.out.println("You can't afford it.");
				}
			} else if (animal == Crocodile.class) {
				if (simulator.canAfford(1200)) {
					simulator.animalList.add(new Crocodile(45, 4, order));
					simulator.spendMoney(1200);
				} else {
					System.out.println("You can't afford it.");
				}
			}
		}
	}

	public Sprite getSprite() {
		return sprite;
	}

}
