package zoo;

import org.jsfml.graphics.Sprite;

public interface Worker {
	public void work(Simulator simulator);

	public void quit();

	public boolean isQuitting();

	public Sprite getSprite();

	public void setOrder(int order);
}
