package zoo;

import java.io.IOException;
import java.nio.file.Paths;
import org.jsfml.graphics.Sprite;
import org.jsfml.graphics.Texture;
import org.jsfml.system.Vector2f;

public class AddReceptionistButton implements Button {

	private Simulator simulator;
	private Texture texture;
	private Sprite sprite;

	public AddReceptionistButton(Simulator simulator) {
		this.simulator = simulator;
		this.texture = new Texture();
		try {
			texture.loadFromFile(Paths.get("sprites/add.png"));
		} catch (IOException ex) {
			ex.printStackTrace();
		}
		this.sprite = new Sprite(texture);
		this.sprite.setOrigin(Vector2f.div(new Vector2f(this.texture.getSize()), 2));
		this.sprite.setPosition(50, 600);
	}

	public void onClick() throws WrongTextureSize {
		int order = 0;
		for (Worker worker : simulator.workers) {
			if (worker.getClass() == Receptionist.class) {
				order++;
			}
		}
		if (order < 10) {
			simulator.workers.add(new Receptionist(order));
		}
	}

	public Sprite getSprite() {
		return sprite;
	}
}
