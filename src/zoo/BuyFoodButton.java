package zoo;

import java.io.IOException;
import java.nio.file.Paths;
import org.jsfml.graphics.Sprite;
import org.jsfml.graphics.Texture;
import org.jsfml.system.Vector2f;

public class BuyFoodButton implements Button {
	private Simulator simulator;
	private Texture texture;
	private Sprite sprite;

	public BuyFoodButton(Simulator simulator) {
		this.simulator = simulator;
		this.texture = new Texture();
		try {
			texture.loadFromFile(Paths.get("sprites/buyfood.png"));
		} catch (IOException ex) {
			ex.printStackTrace();
		}
		this.sprite = new Sprite(texture);
		this.sprite.setOrigin(Vector2f.div(new Vector2f(this.texture.getSize()), 1));
		this.sprite.setPosition(980, 220);
	}

	public void onClick() {
		int meatNecessity = 0;
		int plantsNecessity = 0;

		for (Animal animal : simulator.animalList) {
			if (animal.isCarnivore()) {
				meatNecessity += animal.foodNecessity;
			} else {
				plantsNecessity += animal.foodNecessity;
			}
		}
		for (Animal animal : simulator.animalList) {
			if (animal.isCarnivore() && simulator.getMeatQuantity() < meatNecessity) {
				if (simulator.canAfford(animal.foodNecessity * 100)) {
					simulator.spendMoney(animal.foodNecessity * 100);
					simulator.buyMeat(animal.foodNecessity);
				}
			} else if (simulator.getPlantsQuantity() < plantsNecessity) {
				if (simulator.canAfford(animal.foodNecessity * 50)) {
					simulator.spendMoney(animal.foodNecessity * 50);
					simulator.buyPlants(animal.foodNecessity);
				}
			}
		}
	}

	public Sprite getSprite() {
		return sprite;
	}
}
