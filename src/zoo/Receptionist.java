package zoo;

import java.io.IOException;
import java.nio.file.Paths;

import org.jsfml.graphics.Sprite;
import org.jsfml.graphics.Texture;
import org.jsfml.system.Vector2f;

public class Receptionist implements Worker {

	private int salary;
	private boolean quits;
	private Texture texture;
	private Sprite sprite;
	private int order;

	public Receptionist(int order) throws WrongTextureSize {
		this.salary = 150;
		this.quits = false;
		this.texture = new Texture();
		this.order = order;

		try {
			texture.loadFromFile(Paths.get("sprites/receptionist.png"));
		} catch (IOException ex) {
			ex.getStackTrace();
		}
		if (texture.getSize().x != 64 || texture.getSize().y != 64) {
			throw new WrongTextureSize();
		}
		this.sprite = new Sprite(texture);
		this.sprite.setOrigin(Vector2f.div(new Vector2f(texture.getSize()), 2));
		this.sprite.setPosition(120 + 70 * order, 600);
	}

	public void work(Simulator simulator) {
		if (simulator.canAfford(salary)) {
			System.out.println("Receptionist is working.");
			simulator.spendMoney(salary);
		} else {
			quit();
		}
	}

	public void quit() {
		quits = true;
		System.out.println("Receptionist quits.");
	}

	public boolean isQuitting() {
		return quits;
	}

	public Sprite getSprite() {
		return sprite;
	}

	public void setOrder(int order) {
		this.order = order;
	}
}
