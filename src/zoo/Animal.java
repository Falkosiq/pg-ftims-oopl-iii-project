package zoo;

import org.jsfml.graphics.Sprite;
import org.jsfml.graphics.Texture;

public class Animal {

	protected int popularity;
	protected int foodNecessity;
	protected boolean carnivore;
	protected Texture texture;
	protected Sprite sprite;

	public Animal() {
	}

	public void eat() {
	}

	public void die() {
	}

	public String getInfo() {
		return "Animals are animals.";
	}

	public boolean isCarnivore() {
		return carnivore;
	}

	public Sprite getSprite() {
		return sprite;
	}
}
