package zoo;

public class Bird extends Animal {

	public Bird() {
	}

	@Override
	public String getInfo() {
		return "Birds (class Aves) are a group of endothermic vertebrates, characterised by feathers,"
				+ " a beak with no teeth, the laying of hard-shelled eggs, a high metabolic rate,"
				+ " a four-chambered heart, and a lightweight but strong skeleton."
				+ " Birds live worldwide and range in size from the 5 cm (2 in) bee hummingbird to the 2.75 m (9 ft) ostrich."
				+ " They rank as the class of tetrapods with the most living species, at approximately ten thousand,"
				+ " with more than half of these being passerines, sometimes known as perching birds or, "
				+ " less accurately, as songbirds.";
	}
}
