package zoo;

import org.jsfml.graphics.Sprite;

public interface Button {

	public void onClick() throws WrongTextureSize;

	public Sprite getSprite();
}
