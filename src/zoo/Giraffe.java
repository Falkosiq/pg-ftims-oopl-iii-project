package zoo;

import java.io.IOException;
import java.nio.file.Paths;

import org.jsfml.graphics.Sprite;
import org.jsfml.graphics.Texture;
import org.jsfml.system.Vector2f;

public class Giraffe extends Mammal {

	public Giraffe(int popularity, int foodNecessity, int order) {
		this.popularity = popularity;
		this.foodNecessity = foodNecessity;
		this.carnivore = false;
		this.texture = new Texture();
		try {
			texture.loadFromFile(Paths.get("sprites/giraffe.png"));
		} catch (IOException ex) {
			ex.printStackTrace();
		}
		this.sprite = new Sprite(texture);
		this.sprite.setOrigin(Vector2f.div(new Vector2f(this.texture.getSize()), 2));
		this.sprite.setPosition(120 + 70 * order, 124);
	}

	@Override
	public void eat() {
		System.out.println("Giraffe is eating.");
	}

	@Override
	public void die() {
		System.out.println("Giraffe died.");
	}
}
