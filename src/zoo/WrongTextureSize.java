package zoo;

public class WrongTextureSize extends Exception {
	@Override
	public String getMessage() {
		return "Error! Texture should be 64x64.";
	}
}
