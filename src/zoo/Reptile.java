package zoo;

public class Reptile extends Animal {

	public Reptile() {
	}

	@Override
	public String getInfo() {
		return "Reptiles are a group (Reptilia) of tetrapod animals comprising today's turtles, crocodilians, snakes,"
				+ " lizards, tuatara, and their extinct relatives. The study of these traditional reptile groups,"
				+ " historically combined with that of modern amphibians, is called herpetology."
				+ " Birds are also often included as a sub-group of reptiles by modern scientists.";
	}
}
