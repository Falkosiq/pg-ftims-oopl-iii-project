package zoo;

import java.io.IOException;
import java.nio.file.Paths;
import org.jsfml.graphics.Sprite;
import org.jsfml.graphics.Texture;
import org.jsfml.system.Vector2f;

public class NextDayButton implements Button {
	private Simulator simulator;
	private Texture texture;
	private Sprite sprite;

	public NextDayButton(Simulator simulator) {
		this.simulator = simulator;
		this.texture = new Texture();
		try {
			texture.loadFromFile(Paths.get("sprites/nextday.png"));
		} catch (IOException ex) {
			ex.printStackTrace();
		}
		this.sprite = new Sprite(texture);
		this.sprite.setOrigin(Vector2f.div(new Vector2f(this.texture.getSize()), 1));
		this.sprite.setPosition(980, 307);
	}

	public void onClick() {
		simulator.nextDay();
	}

	public Sprite getSprite() {
		return sprite;
	}
}
