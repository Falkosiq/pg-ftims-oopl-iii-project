package zoo;

import java.io.IOException;
import java.nio.file.Paths;
import java.util.LinkedList;
import org.jsfml.graphics.Color;
import org.jsfml.graphics.Font;
import org.jsfml.graphics.RenderWindow;
import org.jsfml.graphics.Text;
import org.jsfml.system.Vector2f;

public class Render {

	private Simulator simulator;
	private Font font;
	private LinkedList<AddAnimalButton> addButtons;
	private BuyFoodButton buyFood;
	private NextDayButton nextDay;
	private AddReceptionistButton addReceptionist;
	private AddKeeperButton addKeeper;

	public Render(Simulator simulator) {
		this.simulator = simulator;
		font = new Font();
		try {
			font.loadFromFile(Paths.get("fonts/font.ttf"));
		} catch (IOException ex) {
			ex.printStackTrace();
		}

		addButtons = new LinkedList<AddAnimalButton>();

		for (int i = 0; i < 6; i++) {
			Class<?> animal;
			if (i == 0) {
				animal = Lion.class;
			} else if (i == 1) {
				animal = Giraffe.class;
			} else if (i == 2) {
				animal = Seal.class;
			} else if (i == 3) {
				animal = Penguin.class;
			} else if (i == 4) {
				animal = Peacock.class;
			} else {
				animal = Crocodile.class;
			}
			addButtons.add(new AddAnimalButton(simulator, 50 + i * 74, animal));
		}
		buyFood = new BuyFoodButton(simulator);
		nextDay = new NextDayButton(simulator);
		addReceptionist = new AddReceptionistButton(simulator);
		addKeeper = new AddKeeperButton(simulator);
	}

	public void update(RenderWindow window, LinkedList<Animal> animals) {

		draw(window, animals);

	}

	public void draw(RenderWindow window, LinkedList<Animal> animals) {
		for (int i = 0; i < animals.size(); i++) {
			window.draw(animals.get(i).getSprite());
		}
		for (AddAnimalButton button : addButtons) {
			window.draw(button.getSprite());
		}
		window.draw(buyFood.getSprite());
		window.draw(nextDay.getSprite());
		window.draw(addReceptionist.getSprite());
		window.draw(addKeeper.getSprite());
		for (Worker worker : simulator.workers) {
			window.draw(worker.getSprite());
		}
	}

	public void drawText(RenderWindow window, String text, int value, int x, int y) {
		Text data = new Text(text + value, font, 20);
		data.setPosition(x, y);
		data.setColor(Color.BLACK);
		window.draw(data);
	}

	public void click(Vector2f axis, LinkedList<Animal> animals) throws WrongTextureSize {
		for (AddAnimalButton button : addButtons) {
			if (axis.x >= 18 && axis.x <= 82 && axis.y >= button.y - 32 && axis.y <= button.y + 32) {
				button.onClick();
			}
		}
		if (axis.x >= 807 && axis.x <= 980 && axis.y >= 153 && axis.y <= 220) {
			buyFood.onClick();
		}
		if (axis.x >= 807 && axis.x <= 980 && axis.y >= 240 && axis.y <= 307) {
			nextDay.onClick();
		}
		if (axis.x >= 18 && axis.x <= 82 && axis.y >= 568 && axis.y <= 632) {
			addReceptionist.onClick();
		}
		if (axis.x >= 18 && axis.x <= 82 && axis.y >= 642 && axis.y <= 706) {
			addKeeper.onClick();
		}
	}
}
